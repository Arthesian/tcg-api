define({ "api": [
  {
    "type": "post",
    "url": "/Card/:id",
    "title": "Save a new card to the database",
    "name": "Create",
    "group": "Card",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Card",
            "optional": false,
            "field": "the",
            "description": "<p>card to save</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Card",
            "optional": false,
            "field": "Returns",
            "description": "<p>instance of the created card</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/controllers/game.js",
    "groupTitle": "Card"
  },
  {
    "type": "post",
    "url": "/Card/:id",
    "title": "Save a new card to the database",
    "name": "Create",
    "group": "Card",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Card",
            "optional": false,
            "field": "the",
            "description": "<p>card to save</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Card",
            "optional": false,
            "field": "Returns",
            "description": "<p>instance of the created card</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/controllers/card.js",
    "groupTitle": "Card"
  },
  {
    "type": "post",
    "url": "/Card/:id",
    "title": "Save a new card to the database",
    "name": "Create",
    "group": "Card",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Card",
            "optional": false,
            "field": "the",
            "description": "<p>card to save</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Card",
            "optional": false,
            "field": "Returns",
            "description": "<p>instance of the created card</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/controllers/user.js",
    "groupTitle": "Card"
  },
  {
    "type": "post",
    "url": "/Card/:id",
    "title": "Save a new card to the database",
    "name": "Create",
    "group": "Card",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Card",
            "optional": false,
            "field": "the",
            "description": "<p>card to save</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Card",
            "optional": false,
            "field": "Returns",
            "description": "<p>instance of the created card</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/controllers/service.js",
    "groupTitle": "Card"
  },
  {
    "type": "get",
    "url": "/Card/:id",
    "title": "Get specific card by MultiverseId",
    "name": "Get",
    "group": "Card",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Cards unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Card",
            "optional": false,
            "field": "Returns",
            "description": "<p>instance of an {Card}</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/controllers/user.js",
    "groupTitle": "Card"
  },
  {
    "type": "get",
    "url": "/Card/:id",
    "title": "Get specific card by MultiverseId",
    "name": "Get",
    "group": "Card",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Cards unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Card",
            "optional": false,
            "field": "Returns",
            "description": "<p>instance of an {Card}</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/controllers/game.js",
    "groupTitle": "Card"
  },
  {
    "type": "get",
    "url": "/Card/:id",
    "title": "Get specific card by MultiverseId",
    "name": "Get",
    "group": "Card",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Cards unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Card",
            "optional": false,
            "field": "Returns",
            "description": "<p>instance of an {Card}</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/controllers/service.js",
    "groupTitle": "Card"
  },
  {
    "type": "get",
    "url": "/Card/:id",
    "title": "Get specific card by MultiverseId",
    "name": "GetById",
    "group": "Card",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Cards unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Card",
            "optional": false,
            "field": "Returns",
            "description": "<p>instance of an {Card}</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/controllers/card.js",
    "groupTitle": "Card"
  },
  {
    "type": "get",
    "url": "/Card/:id",
    "title": "Request Card information",
    "name": "GetByName",
    "group": "Card",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "Get",
            "description": "<p>a card by it's name</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Card",
            "optional": false,
            "field": "Returns",
            "description": "<p>instance of an {Card}</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/controllers/user.js",
    "groupTitle": "Card"
  },
  {
    "type": "get",
    "url": "/Card/Name/:name",
    "title": "Request Card information",
    "name": "GetByName",
    "group": "Card",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "Get",
            "description": "<p>cards by it's (partial) name</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Card",
            "optional": false,
            "field": "Returns",
            "description": "<p>array of {Card}</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/controllers/card.js",
    "groupTitle": "Card"
  },
  {
    "type": "get",
    "url": "/Card/:id",
    "title": "Request Card information",
    "name": "GetByName",
    "group": "Card",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "Get",
            "description": "<p>a card by it's name</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Card",
            "optional": false,
            "field": "Returns",
            "description": "<p>instance of an {Card}</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/controllers/service.js",
    "groupTitle": "Card"
  },
  {
    "type": "get",
    "url": "/Card/:id",
    "title": "Request Card information",
    "name": "GetByName",
    "group": "Card",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "Get",
            "description": "<p>a card by it's name</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Card",
            "optional": false,
            "field": "Returns",
            "description": "<p>instance of an {Card}</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/controllers/game.js",
    "groupTitle": "Card"
  },
  {
    "type": "put",
    "url": "/Card/:id",
    "title": "Update card information",
    "name": "Update",
    "group": "Card",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Card",
            "optional": false,
            "field": "Instance",
            "description": "<p>of an Card, will be updated based the private _id of the object</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Card",
            "optional": false,
            "field": "Returns",
            "description": "<p>instance of an {Card}</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/controllers/game.js",
    "groupTitle": "Card"
  },
  {
    "type": "put",
    "url": "/Card/:id",
    "title": "Update card information",
    "name": "Update",
    "group": "Card",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Card",
            "optional": false,
            "field": "Instance",
            "description": "<p>of an Card, will be updated based the private _id of the object</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Card",
            "optional": false,
            "field": "Returns",
            "description": "<p>instance of an {Card}</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/controllers/service.js",
    "groupTitle": "Card"
  },
  {
    "type": "put",
    "url": "/Card/:id",
    "title": "Update card information",
    "name": "Update",
    "group": "Card",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Card",
            "optional": false,
            "field": "Instance",
            "description": "<p>of an Card, will be updated based the private _id of the object</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Card",
            "optional": false,
            "field": "Returns",
            "description": "<p>instance of an {Card}</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/controllers/user.js",
    "groupTitle": "Card"
  },
  {
    "type": "post",
    "url": "/Deck/:id",
    "title": "Save a new Deck to the database",
    "name": "Create",
    "group": "Deck",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Deck",
            "optional": false,
            "field": "the",
            "description": "<p>Deck to save</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Deck",
            "optional": false,
            "field": "Returns",
            "description": "<p>instance of the created Deck</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/controllers/deck.js",
    "groupTitle": "Deck"
  },
  {
    "type": "delete",
    "url": "/Deck/:id",
    "title": "Delete a deck",
    "name": "Delete",
    "group": "Deck",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "ID",
            "description": "<p>of the deck to be removed</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Deck",
            "optional": false,
            "field": "Returns",
            "description": "<p>instance of the removed deck</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/controllers/deck.js",
    "groupTitle": "Deck"
  },
  {
    "type": "get",
    "url": "/Deck/:id",
    "title": "Get specific Deck by MultiverseId",
    "name": "Get",
    "group": "Deck",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Decks unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Deck",
            "optional": false,
            "field": "Returns",
            "description": "<p>instance of an {Deck}</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/controllers/deck.js",
    "groupTitle": "Deck"
  },
  {
    "type": "get",
    "url": "/Deck/:id",
    "title": "Get specific Decks by MultiverseIds",
    "name": "GetByIds",
    "group": "Deck",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Id[]",
            "optional": false,
            "field": "Array",
            "description": "<p>of Deck IDs</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "Deleted",
            "description": "<p>boolean wheter to get the deleted decks ( defaults to false )</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Deck[]",
            "optional": false,
            "field": "Decks",
            "description": "<p>Array of Decks</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/controllers/deck.js",
    "groupTitle": "Deck"
  },
  {
    "type": "get",
    "url": "/DeckByName",
    "title": "Get all the decks with a particular (partial) name",
    "name": "GetByName",
    "group": "Deck",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Name",
            "description": "<p>(partial) name of a Deck</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "Deleted",
            "description": "<p>boolean wheter to get the deleted decks ( defaults to false )</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Deck[]",
            "optional": false,
            "field": "Decks",
            "description": "<p>Array of Decks</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/controllers/deck.js",
    "groupTitle": "Deck"
  },
  {
    "type": "get",
    "url": "/DeckByQuery",
    "title": "Search for decks based on specified parameters",
    "name": "GetByQuery",
    "group": "Deck",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Name",
            "description": "<p>(partial) Name of a deck</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "ID",
            "description": "<p>of a player</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Color",
            "description": "<p>color of the deck ( if deck has multiple colors, a single color match will find it)</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "Deleted",
            "description": "<p>boolean wheter to get the deleted decks ( defaults to false )</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Deck[]",
            "optional": false,
            "field": "Decks",
            "description": "<p>Array of Decks ( by default also returns )</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/controllers/deck.js",
    "groupTitle": "Deck"
  },
  {
    "type": "get",
    "url": "/DeckByUser",
    "title": "Get all the decks of a particular user",
    "name": "GetByUserId",
    "group": "Deck",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "ID",
            "description": "<p>of a user ( to retrieve all his/her decks )</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "Deleted",
            "description": "<p>boolean wheter to get the deleted decks ( defaults to false )</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Deck[]",
            "optional": false,
            "field": "Decks",
            "description": "<p>Array of Decks</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/controllers/deck.js",
    "groupTitle": "Deck"
  },
  {
    "type": "put",
    "url": "/Deck/:id",
    "title": "Update Deck information",
    "name": "Update",
    "group": "Deck",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Deck",
            "optional": false,
            "field": "Instance",
            "description": "<p>of an Deck, will be updated based the private _id of the object</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Deck",
            "optional": false,
            "field": "Returns",
            "description": "<p>instance of an {Deck}</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/controllers/deck.js",
    "groupTitle": "Deck"
  }
] });
