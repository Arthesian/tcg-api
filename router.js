var index = require('./api/controllers/index');
var card = require('./api/controllers/card');

module.exports.setup = function (app) {
    app.get('/', index.index);

    // User API Calls
    app.post('/user', user.Create);
    app.get('/user/:id', user.GetById);
    app.get('/user/name/:name', user.GetByName);
    app.put('/user', user.Update);
    app.delete('/user', user.Delete);

    // Card API Calls
    app.post('/card', card.Create);
    app.get('/card/:id', card.GetById);
    app.get('/cardname/:name', card.GetByName);
    app.get('/cardsearch', card.GetByParams);
    app.get('/cardcount', card.GetCount);

    // Deck API Calls
    app.post('/deck', deck.Create);                 // returns Deck
    app.get('/deck/:id', deck.GetById);             // returns Deck
    app.get('/decks/:ids', deck.GetByIds);             // returns Deck
    app.get('/decksbyname/:name', deck.GetByName);  // returns Deck[]
    app.get('/decksofuser/:userid', deck.GetByUserId);      // returns Deck[]
    app.get('/decksbyquery/', deck.GetByQuery);     // returns Deck[]
    app.put('/deck/:deck', deck.Update);                  // returns Deck
    app.delete('/deck/:id', deck.Delete);               // returns Deck

    // // Game API Calls
    // app.post('/game', game.Create);
    // app.get('/game', game.GetById);
    // app.get('/game', game.GetByUserId);
    // app.get('/game', game.GetByDeckId);
    // app.put('/game', game.Update);

    // // stats API Calls
    // app.get('/stats', stats.getStatisticsForUser);
    // app.get('/stats', stats.getStatisticsForCard);
    // app.get('/stats', stats.getStatisticsForDeck);
    // app.get('/stats', stats.getStatisticsForGame);
};