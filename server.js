// server.js

// BASE SETUP
// =============================================================================

var dbUser = 'arthesian';
var dbPass = 'Zto6uI3nCIagJHlHOLSc';

// call the packages we need
var express     = require('express');        // call express
var app         = express();                 // define our app using express
var bodyParser  = require('body-parser');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8888;        // set our port

// Connect to MongoDB
var mongoose = require('mongoose');
var dbUrl = `mongodb://${dbUser}:${dbPass}@cluster0-shard-00-00-hemmp.mongodb.net:27017/Cluster0?authSource=admin&replicaSet=Cluster0-shard-0&ssl=true`;
var db = mongoose.connect(dbUrl, function(err) {
    console.log(err);
});

// ROUTES FOR OUR API
// =============================================================================

var routes = require('./router');
routes.setup(app);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);