var sha256 = require('sha256');

class HashingProvider {

    Hash(input, salt){
        salt = salt || this.GenerateSalt(input);
        sha256.hash(salt + input + salt);
    }

    GenerateSalt(input) {
        return `pNwuQ9${input}Zv7fRzn4DC8tTMh!$Mv@JUWsUR9@xtQv${input}TgCSDcfX!GnJj6SvtrTG7#Hk5jTgu^wJqQrrcD5${input}mW6mUDHM#XMbPE5@5r5k3pZ`;
    }

    IsMatch(input, salt, hash) {
        var inputHash = this.Hash(input, salt);
        return inputHash === hash;
    }
}

module.exports = new HashingProvider();