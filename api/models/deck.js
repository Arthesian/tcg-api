
var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var DeckSchema = new Schema({

    _id: Schema.Types.ObjectId,
    Created: { type: Date, default: Date.now },
    Updated: { type: Date, default: Date.now },
    Name: String,
    Cards: [{Amount: Number, Id: Schema.Types.ObjectId}],
    Owner: Schema.Types.ObjectId,
    ImageUrl : String,
    Colors: [String],
    ManaCurve: [Number],
    Games: Number,
    Wins: Number,
    Draws: Number,
    Loses: Number,
    Deleted: Boolean
});

module.exports = mongoose.model('Deck', DeckSchema, "decks");