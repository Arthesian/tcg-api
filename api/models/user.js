
var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var UserSchema = new Schema({

    _id: Schema.Types.ObjectId,
    Bio: String,
    RegisterDate: Date,
    LastActivityDate: Date,
    Username: String,
    Email: String,
    PasswordHash: String,
    Decks: [Schema.Types.ObjectId]
});

module.exports = mongoose.model('User', UserSchema, "users");