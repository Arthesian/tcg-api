
var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var CardSchema = new Schema({

    _id: Schema.Types.ObjectId,
    Name: String,
    MultiverseId: Number,
    Artist: String,
    ConvertedManaCost: Number,
    Colors: [String],
    ColorIdentity: [String],
    FlavourText: String,
    ImageName: String,
    Layout: String,
    SetName: String,
    BorderColor: String,
    Legalities: [{}],
    ManaCost: String,
    Text: String,
    Power: String,
    Toughness: String,
    Rarity: String,
    TypeText: String,
    Types: [String],
    SubTypes: [String],
    ImageUrl: String,
});

module.exports = mongoose.model('Card', CardSchema, "cards");