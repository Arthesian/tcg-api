var Card = require('./../models/card');

class CardController {

    /**
     * @api {post} /Card/:id Save a new card to the database
     * @apiName Create
     * @apiGroup Card
     *
     * @apiParam {Card} the card to save
     *
     * @apiSuccess {Card} Returns instance of the created card
     */
    Create(req, res) {

        var card = new Card(); // create a new instance of the Card model
        card.name = req.body.name; // set the Cards name (comes from the request)

        // save the Card and check for errors
        card.save(function (err) {
            if (err) {
                res.send(err);
            }

            res.json({
                message: 'Success! Card created!'
            });
        });
    }

    /**
     * @api {get} /Card/:id Get specific card by MultiverseId
     * @apiName GetById
     * @apiGroup Card
     *
     * @apiParam {Number} id Cards unique ID.
     *
     * @apiSuccess {Card} Returns instance of an {Card}
     */
    GetById(req, res) {
        var id = +req.params.id
        Card.find({
            MultiverseId: id
        }).then((cards) => {
            res.json(cards);
        }).catch(err => {
            res.send(err);
        });
    }

    /**
     * @api {get} /Card/Name/:name Request Card information
     * @apiName GetByName
     * @apiGroup Card
     *
     * @apiParam {Number} Get cards by it's (partial) name
     *
     * @apiSuccess {Card} Returns array of {Card}
     */
    GetByName(req, res) {
        Card.find({
            Name: {
                "$regex": req.params.name,
                "$options": "i"
            }
        }).limit(100).then(cards => {
            res.json(cards);
        }).catch(err => {
            res.send(err);
        });
    }

    /**
     * @api {get} /Card/Name/:name Request Card information
     * @apiName GetByName
     * @apiGroup Card
     *
     * @apiParam {Number} Get cards by it's (partial) name
     *
     * @apiSuccess {Card} Returns array of {Card}
     */
    GetByParams(req, res) {

        var params = {}
        Object.assign(params, req.query, req.params);

        var searchObj = {};

        if (params.name) {
            searchObj.Name = {
                "$regex": params.name,
                "$options": "i"
            }
        }

        if (params.flavour) {
            searchObj.FlavourText = {
                "$regex": params.flavour,
                "$options": "i"
            }
        }

        if (params.text) {
            searchObj.Text = {
                "$regex": params.text,
                "$options": "i"
            }
        }

        if (params.artist) {
            searchObj.Artist = {
                "$regex": params.artist,
                "$options": "i"
            }
        }

        if (params.type) {
            searchObj.Types = params.type;
        }

        if (params.subtype) {
            searchObj.SubTypes = params.subtype;
        }

        if (params.cmc) {
            searchObj.ConvertedManaCost = params.cmc;
        }
        
        if (params.rarity) {
            searchObj.Rarity = params.rarity;
        }
        
        if (params.atk) {
            searchObj.Power = params.atk;
        }
        
        if (params.def) {
            searchObj.Toughness = params.def;
        }
        
        if (params.color) {
            searchObj.Colors = params.color;
        }
        
        if (params.colorid) {
            searchObj.ColorIdentity = params.colorid;
        }

        Card.find(searchObj).limit(100).then(cards => {
            res.json(cards);
        }).catch(err => {
            res.send(err);
        });
    }

    /**
     * @api {get} /CardCount Request Card count
     * @apiName GetCount
     * @apiGroup Card
     *
     * @apiSuccess {number} returns the number of cards stored in the database
     */
    GetCount(req, res) {
        Card.count().then(count => {
            res.json(count);
        }).catch(err => {
            res.send(err);
        });
    }
}

module.exports = new CardController();