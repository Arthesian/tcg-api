var User = require('./../models/user');
var HashingHelper = require('./../helpers/hashing');

var responseLimit = 100;

var userController = {

    /**
     * @api {post} /User/:id Save a new User to the database
     * @apiName Create
     * @apiGroup User
     *
     * @apiParam {User} the User to save
     *
     * @apiSuccess {User} Returns instance of the created User
     */
    Create = function (req, res) {

        var params = Object.assign({}, req.body, req.params, req.query);

        var user = new User();      // create a new instance of the Bear model
        user.LastActivityDate = user.RegisterDate = new Date().getTime();
        user.Username = user.Email = '';

        // Generate salt from registerdate
        var dateHash = HashingHelper.Hash(user.RegisterDate);
        var salt = HashingHelper.GenerateSalt(dateHash);

        // Hash password using register date salt
        user.PasswordHash = HashingHelper.Hash(params.password, salt);

        // save the bear and check for errors
        user.save(function (err) {
            if (err)
                res.send(err);

            res.json({ message: 'User created!' });
        });
    },

    /**
     * @api {get} /User/:id Get specific User by MultiverseId
     * @apiName Get
     * @apiGroup User
     *
     * @apiParam {Number} id Users unique ID.
     *
     * @apiSuccess {User} Returns instance of an {User}
     */
    Get = function(req, res) {
        Bear.find(function(err, bears) {
            if (err)
                res.send(err);

            res.json(bears);
        });
    },

    /**
     * @api {put} /User/:id Update User information
     * @apiName Update
     * @apiGroup User
     *
     * @apiParam {User} Instance of an User, will be updated based the private _id of the object
     *
     * @apiSuccess {User} Returns instance of an {User}
     */
    Update = function () {

    },

    /**
     * @api {get} /User/:id Request User information
     * @apiName GetByName
     * @apiGroup User
     *
     * @apiParam {Number} Get a User by it's name
     *
     * @apiSuccess {User} Returns instance of an {User}
     */
    GetByName = function () {

    }
}

module.exports = userController;