var Deck = require('./../models/deck');

var responseLimit = 100;

var deckController = {

    /**
     * @apiDefine Token Credential token received after first login call
     * After calling the 'login' API method, you receive a Token object,
     * which is needed in most API calls to retrieve or update data
     * 
     * @apiParam {String} username 
     * @apiParam {String} password
     */

    /**
     * @apiDefine Credentials Only accessable with user Token
     * This API call is only accessible to logged in users.
     */

    /**
     * @api {post} /Deck/:id Save a new Deck to the database
     * @apiName Create
     * @apiGroup Deck
     *
     * @apiParam {Deck} the Deck to save
     *
     * @apiSuccess {Deck} Returns instance of the created Deck
     */
    Create = function (req, res) {

        var Deck = new Deck();      // create a new instance of the Deck model
        Deck.Name = req.body.name;  // set the Decks name (comes from the request)

        // save the Deck and check for errors
        Deck.save(function (err) {
            if (err)
                res.send(err);

            res.json({ message: 'Deck created!' });
        });
    },

    /**
     * @api {get} /Deck/:id Get specific Deck by MultiverseId
     * @apiName Get
     * @apiGroup Deck
     *
     * @apiParam {Number} id Decks unique ID.
     *
     * @apiSuccess {Deck} Returns instance of an {Deck}
     */
    GetById = function (req, res) {
        Deck.findById(req.params.id).then(deck => {
            res.json(deck);
        }).catch(err => {
            res.send(err);
        });
    },

    /**
     * @api {get} /Deck/:id Get specific Decks by MultiverseIds
     * @apiName GetByIds
     * @apiGroup Deck
     *
     * @apiParam {Id[]} Array of Deck IDs
     * @apiParam {Boolean} Deleted boolean wheter to get the deleted decks ( defaults to false )
     *
     * @apiSuccess {Deck[]} Decks Array of Decks
     */
    GetByIds = function (req, res) {
        var ids = req.params.ids;

        var deleted = !!req.params.deleted;

        Deck.find({
            _id: { $in: ids },
            Deleted: deleted
        }).limit(responseLimit).then(decks => {
            res.json(decks);
        }).catch(err => {
            res.send(err);
        });
    },

    /**
     * @api {get} /DeckByName Get all the decks with a particular (partial) name
     * @apiName GetByName
     * @apiGroup Deck
     *
     * @apiParam {String} Name (partial) name of a Deck
     * @apiParam {Boolean} Deleted boolean wheter to get the deleted decks ( defaults to false )
     *
     * @apiSuccess {Deck[]} Decks Array of Decks
     */
    GetByName = function (req, res) {

        var deleted = !!req.params.deleted;

        Deck.find({
            Name: { $regex: `${reg.params.name}`, $options: 'i' },
            Deleted: deleted
        }).limit(responseLimit).then(decks => {
            res.json(decks);
        }).catch(err => {
            res.send(err);
        });
    },

    /**
     * @api {get} /DeckByUser Get all the decks of a particular user
     * @apiName GetByUserId
     * @apiGroup Deck
     *
     * @apiParam {Object} ID of a user ( to retrieve all his/her decks )
     * @apiParam {Boolean} Deleted boolean wheter to get the deleted decks ( defaults to false )
     *
     * @apiSuccess {Deck[]} Decks Array of Decks
     */
    GetByUserId = function (req, res) {

        var deleted = !!req.params.deleted;

        Deck.find({
            Owner: req.params.userid,
            Deleted: deleted
        }).limit(responseLimit).then(decks => {
            res.json(decks);
        }).catch(err => {
            res.send(err);
        });
    },

    /**
     * @api {get} /DeckByQuery Search for decks based on specified parameters
     * @apiName GetByQuery
     * @apiGroup Deck
     *
     * @apiParam {String} Name (partial) Name of a deck
     * @apiParam {String} ID of a player
     * @apiParam {String} Color color of the deck ( if deck has multiple colors, a single color match will find it)
     * @apiParam {Boolean} Deleted boolean wheter to get the deleted decks ( defaults to false )
     *
     * @apiSuccess {Deck[]} Decks Array of Decks ( by default also returns )
     */
    GetByQuery = function (req, res) {
        var queryParams = {};
        var params = Object.assign({}, req.params, req.query);

        if (params.name) {
            queryParams.Name = { $regex: `${reg.params.name}`, $options: 'i' }
        }

        if (params.color) {
            queryParams.Colors = params.color;
        }

        if (params.owner) {
            queryParams.Owner = params.owner;
        }

        queryParams.Deleted = !!params.deleted;

        Deck.find(queryParams).limit(responseLimit).then(decks => {
            res.json(decks);
        }).catch(err => {
            res.send(err);
        });
    },

    /**
     * @api {put} /Deck/:id Update Deck information
     * @apiName Update
     * @apiGroup Deck
     *
     * @apiParam {Deck} Instance of an Deck, will be updated based the private _id of the object
     *
     * @apiSuccess {Deck} Returns instance of an {Deck}
     */
    Update = function (req, res) {
        Deck.findByIdAndUpdate(req.params.id, req.params.deck).then(deck => {
            res.json(deck);
        }).catch(err => {
            res.send(err);
        });
    },

    /**
     * @api {delete} /Deck/:id Delete a deck
     * @apiName Delete
     * @apiGroup Deck
     *
     * @apiParam {String} ID of the deck to be removed
     *
     * @apiSuccess {Deck} Returns instance of the removed deck
     */
    Delete = function (req, res) {
        Deck.deleteOne({ _id: req.params.id }).then(deck => {
            res.json(deck);
        }).catch(err => {
            res.send(err);
        });
    }
}

module.exports = deckController;