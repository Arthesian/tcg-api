
var cardController = {

    /**
     * @api {post} /Card/:id Save a new card to the database
     * @apiName Create
     * @apiGroup Card
     *
     * @apiParam {Card} the card to save
     *
     * @apiSuccess {Card} Returns instance of the created card
     */
    Create = function (req, res) {

        var bear = new Bear();      // create a new instance of the Bear model
        bear.name = req.body.name;  // set the bears name (comes from the request)

        // save the bear and check for errors
        bear.save(function (err) {
            if (err)
                res.send(err);

            res.json({ message: 'Bear created!' });
        });
    },

    /**
     * @api {get} /Card/:id Get specific card by MultiverseId
     * @apiName Get
     * @apiGroup Card
     *
     * @apiParam {Number} id Cards unique ID.
     *
     * @apiSuccess {Card} Returns instance of an {Card}
     */
    Get = function(req, res) {
        Bear.find(function(err, bears) {
            if (err)
                res.send(err);

            res.json(bears);
        });
    },

    /**
     * @api {put} /Card/:id Update card information
     * @apiName Update
     * @apiGroup Card
     *
     * @apiParam {Card} Instance of an Card, will be updated based the private _id of the object
     *
     * @apiSuccess {Card} Returns instance of an {Card}
     */
    Update = function () {

    },

    /**
     * @api {get} /Card/:id Request Card information
     * @apiName GetByName
     * @apiGroup Card
     *
     * @apiParam {Number} Get a card by it's name
     *
     * @apiSuccess {Card} Returns instance of an {Card}
     */
    GetByName = function () {

    }
}

module.exports = cardController;